#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <unordered_set>
using namespace std;

class Firewall {
private:
    vector<string> direc;
    vector<string> proto;
    int port[65536] = {0};
    unordered_set<string> s;
public:
    void processDirec(string item) {
        if(direc.size() == 2) {
            return;
        }else if(direc.size() == 0) {
            direc.push_back(item);
        } else if(direc[0] != item) {
            direc.push_back(item);
        }
    }
    void processProto(string item) {
        if(proto.size() == 2) {
            return;
        }else if(proto.size() == 0) {
            proto.push_back(item);
        } else if(proto[0] != item) {
            proto.push_back(item);
        }
    }
    void processPort(string item) {
        int pos = item.find('-'), start, ends;
        if(pos != string::npos) {
            start = stoi(item.substr(0, pos));
            ends = stoi(item.substr(pos+1, item.size() - 1 - pos));
            for(int i = start-1; i < ends; i++)
                if(!port[i])
                    port[i] = 1;
        } else {

            port[stoi(item)-1] = 1;
        }
    }

    void processIPaddr(string item) {
        int pos = item.find('-');
        string start, ends;
        if(pos != string::npos) {
            start = item.substr(0, pos);
            ends = item.substr(pos+1, item.size() - 1 - pos);
            string ip[4];
            int pos = start.find('.');
            ip[0]=start.substr(0, pos);
            start = start.substr(pos+1, start.size() - 1- pos);
            pos = start.find('.');
            ip[1] = start.substr(0, pos);
            start = start.substr(pos+1, start.size() - 1 - pos);
            pos = start.find('.');
            ip[2] = start.substr(0, pos);
            ip[3] = start.substr(pos+1, start.size() - 1 - pos);

            while(true) {
                int tmp, carry;
                tmp = stoi(ip[3]);
                tmp++;
                if(tmp == 256) {
                    carry = 1;
                    tmp = 0;
                } else
                    carry = 0;
                ip[3] = to_string(tmp);
                if(carry) {
                    tmp = stoi(ip[2]);
                    tmp++;
                    if(tmp == 256) {
                        carry = 1;
                        tmp = 0;
                    } else
                        carry = 0;
                    ip[2] = to_string(tmp);
                }
                if(carry) {
                    tmp = stoi(ip[1]);
                    tmp++;
                    if(tmp == 256) {
                        carry = 1;
                        tmp = 0;
                    } else
                        carry = 0;
                    ip[1] = to_string(tmp);
                }
                if(carry) {
                    tmp = stoi(ip[0]);
                    tmp++;
                    carry = 0;
                    ip[0] = to_string(tmp);
                }
                string res = ip[0] + '.' + ip[1] + '.' + ip[2] + '.' + ip[3];
                s.insert(res);
                if(res == ends)
                    break;

            }
        } else
            s.insert(item);
    }
    Firewall(string path) {
        string line;
        ifstream file(path);
        file.is_open();
        while(getline(file, line)) {
            string item;
            stringstream ss;
            ss.str(line);
            int i = 0;
            while(getline(ss, item, ',')) {
                switch(i) {
                  case 0:
                    processDirec(item);
                    i++;
                    break;
                  case 1:
                    processProto(item);
                    i++;
                    break;
                  case 2:
                    processPort(item);
                    i++;
                    break;
                  case 3:
                    processIPaddr(item);
                    i++;
                    break;
                }

            }
        }
        file.close();
    }
    bool IsDirection(string d) {
        if(d == direc[0] || (direc.size() > 1 && d == direc[1]))
            return true;
        return false;
    }
    bool IsProtocol(string p) {
        if(p == proto[0] || (proto.size() > 1 && p == proto[1]))
            return true;
        return false;
    }
    bool IsPort(int po) {
        if(port[po-1])
            return true;
        return false;
    }
    bool IsIPaddr(string ip_address) {
        auto it = s.find(ip_address);
        if(it != s.end())
            return true;
        return false;
    }
    bool accept_packet(string direction, string protocol, int port, string ip_address) {
        return IsDirection(direction) && IsProtocol(protocol) && IsPort(port) && IsIPaddr(ip_address);
    }
};
int main() {
    string path = "fw.csv";
    Firewall f(path);
    ifstream input("input.csv");
    ofstream output("output.csv");
    input.is_open();

    string line, item, d, p, ip;
    int po;
    while(getline(input, line)) {
        stringstream sst;
        sst.str(line);
        int i = 0;
        while(getline(sst, item, ',')) {
           switch(i) {
           case 0:
               d = item;
               i++;
               break;
           case 1:
               p = item;
               i++;
               break;
           case 2:
               po = stoi(item);
               i++;
               break;
           case 3:
               ip = item;
               break;
           }
        }
        cout<<d<<" "<<p<<" "<<po<<" "<<ip<<endl;
        if(f.accept_packet(d, p, po, ip))
            output<<"True / Allow"<<endl;
        else
            output<<"False / Don't Allow"<<endl;
    }
    input.close();
    output.close();
    return 0;
}
