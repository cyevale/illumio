# Illumio

The code is written in C++ using object oriented principles.

The files "fw.csv", "input.csv" and "output.csv" must all be in the same directory as the file firewall.cpp. Further, the format for the "input.csv" is same as 
"fw.csv" except that it won't have ranges for port numbers and ip addresses. The output file "output.csv" will save the output correspoding to each line in 
"input.csv" file.

a. For testing, I created an input.csv file with different combinations of the direction, protocol, port number and ip addresses and stored the output for each 
       combination in the output.csv file.
b. For ranges of port number and ip address I chose hash data structure.

The team I am interested in is "Data team".
